package com.cloudcommerce.giftregistry.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Store;

import com.cloudcommerce.giftregistry.tools.GiftRegistryTools;

@Entity(name = "GiftRegistryEvent")
@Table(name = "gift_event_details")
public class GiftRegistryEvent {

	@Id
	@GenericGenerator(parameters=@Parameter(name="sequence" , value="gift_event_details_seq"), strategy="com.cloudcommerce.giftregistry.idgenerator.GiftRegistryIdGenerator", name = "gift_event_details_seq")
	@GeneratedValue(generator = "gift_event_details_seq")
	@Column(name = "event_id")
	private String id;

	@Column(name = "city")
	private String city;

	@Column(name = "state")
	private String state;

	@Column(name = "password")
	private String password;

	@Column(name = "password_protected")
	private String passwordProtected;

	@Column(name = "publish_state")
	private String publishState;

	@Column(name = "event_time")
	private String eventTime;

	@Column(name = "event_date")
	private String eventDate;
	
	@Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
	@Column(name = "event_month")
	private int eventMonth;

	public int getEventMonth() {
		return eventMonth;
	}

	public void setEventMonth(int eventMonth) {
		this.eventMonth = eventMonth;
	}

	public String getId() {
		return GiftRegistryTools.returnEmptyIfNull(id);
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCity() {
		return GiftRegistryTools.returnEmptyIfNull(city);
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return GiftRegistryTools.returnEmptyIfNull(state);
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPassword() {
		return GiftRegistryTools.returnEmptyIfNull(password);
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordProtected() {
		return GiftRegistryTools.returnEmptyIfNull(passwordProtected);
	}

	public void setPasswordProtected(String passwordProtected) {
		this.passwordProtected = passwordProtected;
	}

	public String getPublishState() {
		return GiftRegistryTools.returnEmptyIfNull(publishState);
	}

	public void setPublishState(String publishState) {
		this.publishState = publishState;
	}

	public String getEventTime() {
		return GiftRegistryTools.returnEmptyIfNull(eventTime);
	}

	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}

	public String getEventDate() {
		return GiftRegistryTools.returnEmptyIfNull(eventDate);
	}

	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}

}
