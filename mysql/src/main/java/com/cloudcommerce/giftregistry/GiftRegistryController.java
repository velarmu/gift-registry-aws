package com.cloudcommerce.giftregistry;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudcommerce.giftregistry.model.GiftRegistry;
import com.cloudcommerce.giftregistry.model.GiftRegistryItem;
import com.cloudcommerce.giftregistry.model.GiftRegistryPurchaseDetails;
import com.cloudcommerce.giftregistry.service.GiftRegistryService;
import com.cloudcommerce.giftregistry.service.GiftRegistryServiceImpl;

@Controller
@RequestMapping(value = "/registry")
public class GiftRegistryController {
	private static final Logger logger = LoggerFactory.getLogger(GiftRegistryServiceImpl.class);

	private GiftRegistryService giftRegistryService;

	@Autowired(required = true)
	@Qualifier(value = "giftRegistryService")
	public void setGiftRegistryService(GiftRegistryService ps) {
		this.giftRegistryService = ps;
	}

	@RequestMapping(value = "/addRegistryItem", method = RequestMethod.POST)
	public @ResponseBody GiftRegistryResponse addRegistryItem(@RequestBody GiftRegistryItem giftRegistryItem,
			@RequestParam("registryId") String registryId) {
		Map<String, Object> inputMap = new HashMap<String, Object>();
		GiftRegistryResponse response = new GiftRegistryResponse();
		inputMap.put(Constants.REGISTRY_ID, registryId);
		logger.info("inputMap" + inputMap);
		this.giftRegistryService.addRegistryItem(giftRegistryItem, inputMap, response);
		return response;

	}

	@RequestMapping(value = "/updateRegistryItem", method = RequestMethod.POST)
	public @ResponseBody GiftRegistryResponse updateGiftRegistryItem(@RequestBody GiftRegistryItem giftRegistryItem,
			@RequestParam("registryId") String registryId) {
		Map<String, Object> inputMap = new HashMap<String, Object>();
		GiftRegistryResponse response = new GiftRegistryResponse();
		inputMap.put(Constants.REGISTRY_ID, registryId);
		logger.info("inputMap" + inputMap);
		this.giftRegistryService.updateRegistryItem(giftRegistryItem, inputMap, response);
		return response;
	}

	@RequestMapping(value = "/removeRegistryItem", method = RequestMethod.POST)
	public @ResponseBody GiftRegistryResponse removeGiftRegistryItem(
			@RequestParam("registryItemId") int registryItemId) {
		Map<String, Object> inputMap = new HashMap<String, Object>();
		GiftRegistryResponse response = new GiftRegistryResponse();
		inputMap.put(Constants.REGISTRY_ITEM_ID, registryItemId);
		logger.info("inputMap" + inputMap);
		this.giftRegistryService.removeRegistryItem(inputMap, response);
		return response;
	}

	@RequestMapping(value = "/addRegistry", method = RequestMethod.POST)
	public @ResponseBody GiftRegistryResponse addGiftRegistry(@RequestBody GiftRegistry giftRegistry,
			@RequestParam("userId") String userId) {
		Map<String, Object> inputMap = new HashMap<String, Object>();
		GiftRegistryResponse response = new GiftRegistryResponse();
		inputMap.put(Constants.USER_ID, userId);
		logger.info("inputMap" + inputMap);
		this.giftRegistryService.addRegistry(giftRegistry, inputMap, response);
		return response;
	}
	
	@RequestMapping(value = "/addPurchaseDetails", method = RequestMethod.POST)
	public @ResponseBody GiftRegistryResponse purchaseDetails(@RequestBody GiftRegistryPurchaseDetails PurchaseDetails,
	@RequestParam("registryId") String registryId) {
		System.out.println("entered controller");
		Map<String, Object> inputMap = new HashMap<String, Object>();
		GiftRegistryResponse response = new GiftRegistryResponse();
		inputMap.put(Constants.REGISTRY_ID, registryId);
		logger.info("inputMap" + inputMap);
		this.giftRegistryService.addPurchaseDetails(PurchaseDetails, inputMap, response);
		return response;
	}

	@RequestMapping(value = "/updateRegistry", method = RequestMethod.POST)
	public @ResponseBody GiftRegistryResponse updateGiftRegistry(@RequestBody GiftRegistry giftRegistry,
			@RequestParam("userId") String userId) {
		Map<String, Object> inputMap = new HashMap<String, Object>();
		GiftRegistryResponse response = new GiftRegistryResponse();
		inputMap.put(Constants.USER_ID, userId);
		logger.info("inputMap" + inputMap);
		this.giftRegistryService.updateRegistry(giftRegistry, inputMap, response);
		return response;
	}
	
	@RequestMapping(value = "/updateRegistryWithItem", method = RequestMethod.POST)
	public @ResponseBody GiftRegistryResponse updateGiftRegistryWithItem(@RequestBody GiftRegistry giftRegistry,
			@RequestParam("userId") String userId) {
		Map<String, Object> inputMap = new HashMap<String, Object>();
		GiftRegistryResponse response = new GiftRegistryResponse();
		inputMap.put(Constants.USER_ID, userId);
		logger.info("inputMap" + inputMap);
		this.giftRegistryService.updateRegistryWithItem(giftRegistry, inputMap, response);
		return response;
	}

	@RequestMapping(value = "/getRegistry", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody GiftRegistryResponse getGiftRegistry(@RequestParam("registryId") String registryId) {
		Map<String, Object> inputMap = new HashMap<String, Object>();
		GiftRegistryResponse response = new GiftRegistryResponse();
		inputMap.put(Constants.REGISTRY_ID, registryId);
		logger.info("inputMap" + inputMap);
		this.giftRegistryService.getGiftRegistryById(inputMap, response);
		System.out.println("getRegistry=" + response.getResultObjects());
		return response;

	}
	
	@RequestMapping(value = "/getPurchaseReport", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody  GiftRegistryResponse getPurchaseReport(@RequestParam("userId") String userId) {
		Map<String, Object> inputMap = new HashMap<String, Object>();
		GiftRegistryResponse response = new GiftRegistryResponse();
		inputMap.put(Constants.USER_ID, userId);
		logger.info("inputMap" + inputMap);
		this.giftRegistryService.getPurchaseReport(inputMap, response);
		System.out.println("Report=" + response.getResultObjects());
		return response;

	}

	@RequestMapping(value = "/getRegistryByUser", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody GiftRegistryResponse getGiftRegistryByUser(@RequestParam("userId") String userId) {
		Map<String, Object> inputMap = new HashMap<String, Object>();
		GiftRegistryResponse response = new GiftRegistryResponse();
		inputMap.put(Constants.USER_ID, userId);
		logger.info("inputMap" + inputMap);
		this.giftRegistryService.getGiftRegistriesByUserId(inputMap, response);
		System.out.println("getRegistry=" + response.getResultObjects());
		return response;

	}

	@RequestMapping(value = "/removeRegistry", method = RequestMethod.POST)
	public @ResponseBody GiftRegistryResponse removeGiftRegistry(@RequestParam("registryId") String registryId) {
		Map<String, Object> inputMap = new HashMap<String, Object>();
		GiftRegistryResponse response = new GiftRegistryResponse();
		inputMap.put(Constants.REGISTRY_ID, registryId);
		logger.info("inputMap" + inputMap);
		this.giftRegistryService.removeGiftRegistry(inputMap, response);
		return response;
	}

	@RequestMapping(value = "/searchRegistry", method = RequestMethod.POST)
	public @ResponseBody GiftRegistryResponse searchGiftRegistry(@RequestBody SearchGiftRegistry searchRegistry) {
		Map<String, Object> inputMap = new HashMap<String, Object>();
		GiftRegistryResponse response = new GiftRegistryResponse();
		logger.info("inputMap" + inputMap);
		this.giftRegistryService.searchGiftRegistry(searchRegistry, inputMap, response);
		return response;

	}

	@RequestMapping(value = "/indexSearch", method = RequestMethod.GET)
	public @ResponseBody GiftRegistryResponse indexSearch() {
		Map<String, Object> inputMap = new HashMap<String, Object>();
		GiftRegistryResponse response = new GiftRegistryResponse();
		logger.info("inputMap" + inputMap);
		this.giftRegistryService.indexSearchRecords(inputMap, response);
		return response;

	}

	@RequestMapping(value = "/updateGiftItemPurchase", method = RequestMethod.POST)
	public @ResponseBody GiftRegistryResponse updateGiftItemPurchaseDetails(@RequestBody String orderJson) {
		GiftRegistryResponse response = new GiftRegistryResponse();
		Map<String, Object> inputMap = new HashMap<String, Object>();
		inputMap.put(Constants.ORDER_JSON_KEY, orderJson);
		giftRegistryService.updateGiftItemPurchaseDetails(inputMap, response);
		return response;
	}

	@RequestMapping(value = "/getBasicRegistryDetails", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody GiftRegistryResponse getBasicRegistryDetails(@RequestParam("userId") String userId) {
		Map<String, Object> inputMap = new HashMap<String, Object>();
		GiftRegistryResponse response = new GiftRegistryResponse();
		inputMap.put(Constants.USER_ID, userId);
		logger.info("inputMap" + inputMap);
		this.giftRegistryService.getBasicGiftRegistriesByUserId(inputMap, response);
		System.out.println("getBasicRegistry=" + response.getResultObjects());
		return response;

	}
	
	@RequestMapping(value = "/updateBasicItemDetails", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody GiftRegistryResponse updateBasicItemDetails(@RequestBody GiftRegistryItem giftRegistryItem,
			@RequestParam("itemId") String itemId) {
		Map<String, Object> inputMap = new HashMap<String, Object>();
		GiftRegistryResponse response = new GiftRegistryResponse();
		inputMap.put(Constants.ITEM_ID, itemId);
		logger.info("inputMap" + inputMap);
		this.giftRegistryService.updateBasicItemDetails(giftRegistryItem,inputMap, response);
		System.out.println("getBasicRegistry=" + response.getResultObjects());
		return response;

	}
	
	
	
	


	@RequestMapping(value = "/test", consumes = (MediaType.APPLICATION_JSON_VALUE), method = RequestMethod.POST)
	public String listPersons() {
		System.out.println("testing");
		return "success";
	}

}
