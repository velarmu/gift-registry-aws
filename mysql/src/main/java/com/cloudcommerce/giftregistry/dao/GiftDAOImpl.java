package com.cloudcommerce.giftregistry.dao;

import java.math.BigInteger;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.lucene.search.Query;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.internal.SessionImpl;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.type.StandardBasicTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cloudcommerce.giftregistry.Constants;
import com.cloudcommerce.giftregistry.GiftRegistryItemPurchaseDetails;
import com.cloudcommerce.giftregistry.GiftRegistryItemPurchaseReport;
import com.cloudcommerce.giftregistry.GiftRegistryPurchaseReport;
import com.cloudcommerce.giftregistry.GiftRegistryResponse;
import com.cloudcommerce.giftregistry.SearchGiftRegistry;
import com.cloudcommerce.giftregistry.model.GiftRegistry;
import com.cloudcommerce.giftregistry.model.GiftRegistryItem;
import com.cloudcommerce.giftregistry.model.GiftRegistryPurchaseDetails;
import com.cloudcommerce.giftregistry.model.GiftRegistryUser;
import com.cloudcommerce.giftregistry.tools.GiftRegistryTools;

@Repository
@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
public class GiftDAOImpl implements GiftDAO {

	private static final Logger logger = LoggerFactory.getLogger(GiftDAOImpl.class);

	@Autowired
	@Qualifier(value = "hibernate4AnnotatedSessionFactoryLocal")
	private SessionFactory sessionFactoryLocal;

	@Autowired
	@Qualifier(value = "hibernate4AnnotatedSessionFactoryAWS")
	private SessionFactory sessionFactoryAWS;

	public SessionFactory getSessionFactoryLocal() {
		return sessionFactoryLocal;
	}

	public void setSessionFactoryLocal(SessionFactory sessionFactoryLocal) {
		this.sessionFactoryLocal = sessionFactoryLocal;
	}

	public SessionFactory getSessionFactoryAWS() {
		return sessionFactoryAWS;
	}

	public void setSessionFactoryAWS(SessionFactory sessionFactoryAWS) {
		this.sessionFactoryAWS = sessionFactoryAWS;
	}

	boolean calledFromAdd;

	@Override
	public void updateRegistryItem(GiftRegistryItem giftRegistryItem, Map<String, Object> inputMap,
			GiftRegistryResponse response) {
		String registryId = (String) inputMap.get(Constants.REGISTRY_ID);
		String clientId = (String) inputMap.get(Constants.CLIENT_ID);
		Session session = getSessionFactoryForClientId(clientId).getCurrentSession();
		GiftRegistry giftRegistry = (GiftRegistry) session.get(GiftRegistry.class, registryId);
		if (giftRegistry != null && giftRegistry.getId() != null) {
			giftRegistryItem.setGiftRegistry(giftRegistry);
			session.merge(giftRegistryItem);
			logger.info("updateRegistryItem successful:" + giftRegistryItem);
			response.setProcessedRegistryOrItemId((giftRegistryItem.getId()));
		} else {
			logger.info("updateRegistryItem failure. invalid registryId=" + registryId);
			response.getErrorMessages().add("No registry found with id=" + registryId);
		}
	}

	@Override
	public void removeRegistryItem(Map<String, Object> inputMap, GiftRegistryResponse response) {
		Integer registryItemId = (Integer) inputMap.get(Constants.REGISTRY_ITEM_ID);
		String clientId = (String) inputMap.get(Constants.CLIENT_ID);
		Session session = getSessionFactoryForClientId(clientId).getCurrentSession();
		GiftRegistryItem giftRegistryItem = (GiftRegistryItem) session.get(GiftRegistryItem.class, registryItemId);
		if (giftRegistryItem != null && !GiftRegistryTools.isBlank(giftRegistryItem.getId())) {
			giftRegistryItem.getGiftRegistry().getGiftRegistryItems().remove(giftRegistryItem);
			session.delete(giftRegistryItem);
			logger.info("removeRegistryItem successful:" + giftRegistryItem);
		} else {
			logger.info("removeRegistryItem failure. invalid registryItemId=" + registryItemId);
			response.getErrorMessages().add("No registry item found with id=" + registryItemId);
		}
	}

	@Override
	public void addRegistryItem(GiftRegistryItem giftRegistryItem, Map<String, Object> inputMap,
			GiftRegistryResponse response) {
		String registryId = (String) inputMap.get(Constants.REGISTRY_ID);
		String clientId = (String) inputMap.get(Constants.CLIENT_ID);
		Session session = getSessionFactoryForClientId(clientId).getCurrentSession();
		GiftRegistry giftRegistry = (GiftRegistry) session.get(GiftRegistry.class, registryId);
		boolean giftRegistryItemSkuExists = false;
		if (giftRegistry != null && giftRegistry.getId() != null) {
			giftRegistryItem.setGiftRegistry(giftRegistry);
			giftRegistryItemSkuExists = checkAndUpdateGiftRegistryItem(giftRegistryItem, session, giftRegistry);
			if (!giftRegistryItemSkuExists) {
				logger.info("adding new gift registryItem with sku id=" + giftRegistryItem.getSkuId());
				session.merge((giftRegistryItem));
				response.setProcessedRegistryOrItemId(((giftRegistryItem.getId())));
			}
			response.setProcessedRegistryOrItemId(((giftRegistryItem.getId())));
			logger.info("addRegistryItem successful:" + giftRegistryItem);
		}

		else {
			logger.info("addRegistryItem failure. invalid registryId=" + registryId);
			response.getErrorMessages().add("No registry found with id=" + registryId);
		}
	}

	@Override
	public void addGiftRegistry(GiftRegistry giftRegistry, Map<String, Object> inputMap,
			GiftRegistryResponse response) throws HibernateException, SQLException {
		String userId = (String) inputMap.get(Constants.USER_ID);
		String clientId = (String) inputMap.get(Constants.CLIENT_ID);
		String registryId;

		ArrayList<GiftRegistryItem> giftRegistryItem = new ArrayList<GiftRegistryItem>();

		if (!(giftRegistry.getGiftRegistryItems() == null) && giftRegistry.getGiftRegistryItems().size() > 0) {
			for (GiftRegistryItem item : giftRegistry.getGiftRegistryItems()) {

				giftRegistryItem.add(item);

			}
			giftRegistry.getGiftRegistryItems().removeAll(giftRegistryItem);
		}
		Session session = getSessionFactoryForClientId(clientId).getCurrentSession();
		String generatedId = null;
		generatedId = GiftRegistryTools.getGeneratedId("gift_registry_det_seq", ((SessionImpl) session).connection(),
				Constants.EMPTY_STRING);
		System.out.println(generatedId);

		giftRegistry.setId(generatedId);

		GiftRegistryUser giftRegistryUser = (GiftRegistryUser) session.get(GiftRegistryUser.class, userId);

		if (giftRegistryUser != null && !GiftRegistryTools.isBlank(giftRegistryUser.getId())) {
			giftRegistry.setGiftRegistryUser(giftRegistryUser);
			session.persist(giftRegistry);
			logger.info("addGiftRegistry successful:" + giftRegistry);
			registryId = giftRegistry.getId();
		} else {
			GiftRegistryUser newUser = new GiftRegistryUser();
			newUser.setId(userId);
			session.persist(newUser);
			logger.info("user not found for given userId. hence created new one  " + newUser.getId());
			giftRegistry.setGiftRegistryUser(newUser);
			session.persist(giftRegistry);
			logger.info("addGiftRegistry successful:" + giftRegistry);
			registryId = giftRegistry.getId();
		}

		if (!(giftRegistry.getGiftRegistryItems() == null) && giftRegistryItem.size() > 0) {
			// calledFromAdd = true;
			inputMap.put(Constants.REGISTRY_ID, registryId);
			for (GiftRegistryItem item : giftRegistryItem) {
				updateRegistryItem(item, inputMap, response);
			}
		}

		response.setProcessedRegistryOrItemId(registryId);
	}

	@Override
	public void updateRegistry(GiftRegistry giftRegistry, Map<String, Object> inputMap, GiftRegistryResponse response) {
		String userId = (String) inputMap.get(Constants.USER_ID);
		String clientId = (String) inputMap.get(Constants.CLIENT_ID);
		Session session = getSessionFactoryForClientId(clientId).getCurrentSession();
		GiftRegistryUser giftRegistryUser = (GiftRegistryUser) session.get(GiftRegistryUser.class, userId);
		if (giftRegistryUser != null && !GiftRegistryTools.isBlank(giftRegistryUser.getId())) {
			giftRegistry.setGiftRegistryUser(giftRegistryUser);
			session.merge(giftRegistry);
			logger.info("updateRegistry successful:" + giftRegistry);
			response.setProcessedRegistryOrItemId(giftRegistry.getId());
		} else {

			GiftRegistryUser newUser = new GiftRegistryUser();
			newUser.setId(userId);
			session.persist(newUser);
			logger.info("user not found for given userId. hence created new one" + newUser.getId());
			updateRegistry(giftRegistry, inputMap, response);
		}
	}

	@Override
	public GiftRegistry getGiftRegistryById(Map<String, Object> inputMap, GiftRegistryResponse response) {
		String registryId = (String) inputMap.get(Constants.REGISTRY_ID);
		String clientId = (String) inputMap.get(Constants.CLIENT_ID);
		Session session = getSessionFactoryForClientId(clientId).getCurrentSession();
		GiftRegistry giftRegistry = (GiftRegistry) session.get(GiftRegistry.class, registryId);
		logger.info("getGiftRegistryById:" + giftRegistry);
		if (giftRegistry == null || giftRegistry.getId() == null) {
			logger.info("getGiftRegistryById not successful. invalid registryId:" + registryId);
			response.getErrorMessages().add("No registry found with id=" + registryId);
		}
		return giftRegistry;
	}

	@Override
	public GiftRegistryUser getGiftRegistryUser(Map<String, Object> inputMap, GiftRegistryResponse response) {
		String userId = (String) inputMap.get(Constants.USER_ID);
		String clientId = (String) inputMap.get(Constants.CLIENT_ID);
		Session session = getSessionFactoryForClientId(clientId).getCurrentSession();
		GiftRegistryUser giftRegistryUser = (GiftRegistryUser) session.get(GiftRegistryUser.class, userId);
		logger.info("getGiftRegistryById:" + giftRegistryUser);
		if (giftRegistryUser == null || GiftRegistryTools.isBlank(giftRegistryUser.getId())) {
			logger.info("getGiftRegistryById not successful. invalid userId:" + userId);
			response.getErrorMessages().add("No user found with id=" + userId);
		}
		return giftRegistryUser;
	}

	@Override
	public void removeGiftRegistry(Map<String, Object> inputMap, GiftRegistryResponse response) {
		String registryId = (String) inputMap.get(Constants.REGISTRY_ID);
		String clientId = (String) inputMap.get(Constants.CLIENT_ID);
		Session session = getSessionFactoryForClientId(clientId).getCurrentSession();
		GiftRegistry giftRegistry = (GiftRegistry) session.get(GiftRegistry.class, registryId);
		if (giftRegistry != null && giftRegistry.getId() != null) {
			giftRegistry.getGiftRegistryUser().getGiftRegistries().remove(giftRegistry);
			session.delete(giftRegistry);
			logger.info("getGiftRegistryById successful:" + giftRegistry);
		} else {
			logger.info("removeGiftRegistry failure. invalid registryId=" + registryId);
			response.getErrorMessages().add("No registry found with id=" + registryId);
		}
	}

	@Override
	public List searchGiftRegistry(SearchGiftRegistry searchRegistry, Map<String, Object> inputMap,
			GiftRegistryResponse response) {
		String clientId = (String) inputMap.get(Constants.CLIENT_ID);
		Session session = getSessionFactoryForClientId(clientId).getCurrentSession();
		Criteria cr = null;

		cr = session.createCriteria(GiftRegistry.class);
		Criterion id = null;
		Criterion type = null;
		Criterion month = null;

		if (!GiftRegistryTools.isBlank(searchRegistry.getRegistryId())) {

			cr.add(Restrictions.eq("id", searchRegistry.getRegistryId()));
		}

		if (!GiftRegistryTools.isBlank(searchRegistry.getRegistryType())) {

			type = Restrictions.eq("type", searchRegistry.getRegistryType());
			cr.add(type);
		}

		if (searchRegistry.getEventMonth() > 0) {
			cr.createAlias("event", "event").add(Restrictions.eq("event.eventMonth", searchRegistry.getEventMonth()));
		}

		cr.createAlias("coRegistrant", "coRegistrant");
		cr.createAlias("primaryRegistrant", "primaryRegistrant");

		if (!GiftRegistryTools.isBlank(searchRegistry.getFirstName())) {

			Disjunction or = Restrictions.disjunction();
			or.add(Restrictions.ilike("coRegistrant.firstName", "%" + searchRegistry.getFirstName() + "%"));
			or.add(Restrictions.ilike("primaryRegistrant.firstName", "%" + searchRegistry.getFirstName() + "%"));
			cr.add(or);
		}

		if (!GiftRegistryTools.isBlank(searchRegistry.getLastName())) {
			Disjunction or1 = Restrictions.disjunction();
			or1.add(Restrictions.ilike("coRegistrant.lastName", "%" + searchRegistry.getLastName() + "%"));
			or1.add(Restrictions.ilike("primaryRegistrant.lastName", "%" + searchRegistry.getLastName() + "%"));
			cr.add(or1);
		}

		List Results = cr.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();

		logger.info("search registry successful" + Results);
		return Results;
	}

	@Override
	public void indexSearchRecords(Map<String, Object> inputMap) throws Exception {
		String clientId = (String) inputMap.get(Constants.CLIENT_ID);
		Session session = getSessionFactoryForClientId(clientId).getCurrentSession();
		FullTextSession fullTextSession = Search.getFullTextSession(session);
		fullTextSession.createIndexer().startAndWait();
	}

	@Override
	public void updateGiftItemPurchaseDetails(Map<String, Object> inputMap, GiftRegistryResponse response) {
		List<GiftRegistryItemPurchaseDetails> purchaseDetailsList = (List<GiftRegistryItemPurchaseDetails>) inputMap
				.get(Constants.PURCHASE_DETAILS);
		if (purchaseDetailsList != null && purchaseDetailsList.size() > 0) {
			String clientId = (String) inputMap.get(Constants.CLIENT_ID);
			Session session = getSessionFactoryForClientId(clientId).getCurrentSession();
			for (GiftRegistryItemPurchaseDetails purchaseDetails : purchaseDetailsList) {
				String registryId = purchaseDetails.getRegistryId();
				GiftRegistry giftRegistry = (GiftRegistry) session.get(GiftRegistry.class, registryId);
				if (giftRegistry != null && giftRegistry.getId() != null) {
					checkAndUpdateGiftItemPurchaseDetails(purchaseDetails, session, giftRegistry);
				} else {
					logger.info("updateGiftItemPurchaseDetails failure. invalid registryId=" + registryId);
					response.getErrorMessages().add("No registry found with id=" + registryId);
				}
			}
		}
	}


	private boolean checkAndUpdateGiftRegistryItem(GiftRegistryItem giftRegistryItem, Session session,
			GiftRegistry giftRegistry) {
		boolean giftRegistryItemSkuExists = false;
		Set<GiftRegistryItem> giftRegistryItems = giftRegistry.getGiftRegistryItems();
		if (giftRegistryItems != null && giftRegistryItems.size() > 0) {
			for (GiftRegistryItem savedGiftRegistryItem : giftRegistryItems) {
				if (!GiftRegistryTools.isBlank(savedGiftRegistryItem.getSkuId())
						&& savedGiftRegistryItem.getSkuId().equals(giftRegistryItem.getSkuId())) {
					logger.info("sku id which exists in existing registryItem=" + savedGiftRegistryItem.getSkuId());
					logger.info("id of existing registryItem=" + savedGiftRegistryItem.getId());
					giftRegistryItem.setId(savedGiftRegistryItem.getId());
					giftRegistryItem.setRequestedQuantity(
							giftRegistryItem.getRequestedQuantity() + savedGiftRegistryItem.getRequestedQuantity());
					giftRegistryItem.setReceivedQuantity(savedGiftRegistryItem.getReceivedQuantity());
					session.merge(giftRegistryItem);

					giftRegistryItemSkuExists = true;
					break;
				}
			}
		}
		return giftRegistryItemSkuExists;
	}

	private void checkAndUpdateGiftItemPurchaseDetails(GiftRegistryItemPurchaseDetails giftItemPurchaseDetails,
			Session session, GiftRegistry giftRegistry) {
		Set<GiftRegistryItem> giftRegistryItems = giftRegistry.getGiftRegistryItems();
		if (giftRegistryItems != null && giftRegistryItems.size() > 0) {
			for (GiftRegistryItem savedGiftRegistryItem : giftRegistryItems) {
				if (!GiftRegistryTools.isBlank(savedGiftRegistryItem.getSkuId())
						&& savedGiftRegistryItem.getSkuId().equals(giftItemPurchaseDetails.getSkuId())) {
					logger.info("sku id which exists for update purchase item=" + savedGiftRegistryItem.getSkuId());
					logger.info("id of existing registryItem=" + savedGiftRegistryItem.getId());
					savedGiftRegistryItem.setGiftRegistry(giftRegistry);
					savedGiftRegistryItem.setReceivedQuantity(giftItemPurchaseDetails.getReceivedQuantity()
							+ savedGiftRegistryItem.getReceivedQuantity());
					session.merge(savedGiftRegistryItem);
					break;
				}
			}
		}
	}

	public String returnNewId(String id, String sequence) {

		String clientId = Constants.CLIENT_ID;
		Session session = getSessionFactoryForClientId(clientId).getCurrentSession();
		String seq = sequence + "." + "nextval";
		SQLQuery query = session.createSQLQuery("select " + seq + " as string from dual").addScalar("string",
				StandardBasicTypes.STRING);
		String i = ((String) query.uniqueResult()).toString();
		return id + i;

	}

	public Long getNext() {

		// String clientId = (String) inputMap.get(Constants.CLIENT_ID);
		Session session = getSessionFactoryForClientId("clientId").getCurrentSession();
		Query query = (Query) session.createSQLQuery("select gift_registry_det_seq.nextval as num from dual")
				.addScalar("num", StandardBasicTypes.BIG_INTEGER);

		return ((BigInteger) ((org.hibernate.Query) query).uniqueResult()).longValue();
	};

	private SessionFactory getSessionFactoryForClientId(String clientId) {
		if (Constants.CLIENT_AWS.equalsIgnoreCase(clientId)) {
			return this.sessionFactoryAWS;
		} else {
			return this.sessionFactoryAWS;
		}
	}

	@Override
	public void addPurchaseDetails(GiftRegistryPurchaseDetails purchaseDetails, Map<String, Object> inputMap,
			GiftRegistryResponse response) {
		String registryId = (String) inputMap.get(Constants.REGISTRY_ID);
		String clientId = (String) inputMap.get(Constants.CLIENT_ID);
		Session session = getSessionFactoryForClientId(clientId).getCurrentSession();
		GiftRegistry giftRegistry = (GiftRegistry) session.get(GiftRegistry.class, registryId);
		if (giftRegistry != null && purchaseDetails != null) {
			purchaseDetails.setGiftRegistry(giftRegistry);
			session.merge(purchaseDetails);
		} else {
			logger.info("addPurchaseDetails failure. invalid registryId=" + registryId);
			response.getErrorMessages().add("No registry found with id=" + registryId);
		}
	}

	@Override
	public GiftRegistryPurchaseDetails getPurchaseDetails(Map<String, Object> inputMap,
			GiftRegistryResponse response) {
		String purchaseId = (String) inputMap.get(Constants.PURCHASE_ID);
		String clientId = (String) inputMap.get(Constants.CLIENT_ID);
		Session session = getSessionFactoryForClientId(clientId).getCurrentSession();
		GiftRegistryPurchaseDetails purchaseDetails = (GiftRegistryPurchaseDetails) session.get(GiftRegistryPurchaseDetails.class, purchaseId);
		if (purchaseDetails != null) {

		
		} else {
			logger.info("getPurchaseDetails failure. invalid purchaseId=" + purchaseDetails.getId());
			response.getErrorMessages().add("No details  found with id=" + purchaseDetails.getId());
		}
		
		return purchaseDetails;
	}

	
	@Override
	public void updateBasicItemDetails(GiftRegistryItem giftRegistryItem, Map<String, Object> inputMap,
			GiftRegistryResponse response) {

		String itemId = (String) inputMap.get(Constants.ITEM_ID);
		String clientId = (String) inputMap.get(Constants.CLIENT_ID);
		Session session = getSessionFactoryForClientId(clientId).getCurrentSession();
		GiftRegistryItem item = (GiftRegistryItem) session.get(GiftRegistryItem.class, itemId);

		if (item != null && item.getId() != null) {
			item.setCategory(item.getCategory());
			item.setDisplayName(item.getDisplayName());
			item.setId(item.getId());
			item.setImageURL(item.getImageURL());
			item.setListPrice(item.getListPrice());
			item.setProductId(item.getProductId());
			item.setProductURL(item.getProductURL());
			item.setSalePrice(item.getSalePrice());
			item.setSkuId(item.getSkuId());
			item.setReceivedQuantity(item.getReceivedQuantity());

			if ((GiftRegistryTools.isBlank(giftRegistryItem.getFavorite()))) {
				item.setFavorite(item.getFavorite());
			} else {
				item.setFavorite(giftRegistryItem.getFavorite());
			}

			if ((GiftRegistryTools.isBlank(giftRegistryItem.getFavoriteMessage()))) {
				item.setFavoriteMessage(item.getFavoriteMessage());
			} else {
				item.setFavoriteMessage(giftRegistryItem.getFavoriteMessage());
			}

			if (giftRegistryItem.getRequestedQuantity() > 0) {
				item.setRequestedQuantity(giftRegistryItem.getRequestedQuantity());
			} else {

				item.setRequestedQuantity(item.getRequestedQuantity());
			}

			session.merge(item);
			logger.info("updateBasicItemDetails successful:" + giftRegistryItem);
			response.setProcessedRegistryOrItemId((giftRegistryItem.getId()));
		} else {
			logger.info("updateBasicItemDetails failure. invalid itemId=" + itemId);
			response.getErrorMessages().add("No item found with id=" + itemId);
		}

	}

	@Override
	public List<GiftRegistryPurchaseReport> getPurchaseReport(Map<String, Object> inputMap,
			GiftRegistryResponse response) {
		
		GiftRegistryUser user =  getGiftRegistryUser(inputMap,response);
		
		 List<GiftRegistryPurchaseReport> reportList = new  ArrayList<GiftRegistryPurchaseReport>();
		
		if (response.getErrorMessages().size() == 0)
		{
			for (GiftRegistry reg : user.getGiftRegistries()) {
				GiftRegistryPurchaseReport registry = new GiftRegistryPurchaseReport();
				if (reg != null && reg.getGiftRegistryItems().size() > 0)
				{
					registry.setRegistryId(reg.getId());
					GiftRegistryItemPurchaseReport item  = null;
					
					
					for (GiftRegistryPurchaseDetails itemReport :  reg.getPurchaseDetails()) 
					{
						item = new GiftRegistryItemPurchaseReport();
						item.setSkuId(itemReport.getSkuId());
						item.setItemName(itemReport.getItemName());
						item.setShippingAddress(reg.getPostEventShipping().getAddress1() + reg.getPostEventShipping().getAddress2());
					    item.setPurchaseDate(itemReport.getPurchaseDate());
					    item.setQuantity(itemReport.getQuantity());
						item.setStorePurchased(itemReport.getStoreId());
						item.setPurchaserName(itemReport.getFirstName() + itemReport.getLastName());
						item.setTrackingNumber(itemReport.getTrackingNumber());
						registry.getItemReports().add(item);
						
						
					}
					
				
					
				}
				else
				{
				   response.getErrorMessages().add("Could not retrieve items with registryid = "+ reg.getId() );
				}
				
				
				
				reportList.add(registry);
				
			}
		}
		else
		{
			response.getErrorMessages().add("No registries exist for the user = "+ user.getId());
		}
	
            

		

		return reportList;
	}

}
