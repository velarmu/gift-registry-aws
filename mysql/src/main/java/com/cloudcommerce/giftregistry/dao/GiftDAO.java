package com.cloudcommerce.giftregistry.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;

import com.cloudcommerce.giftregistry.GiftRegistryPurchaseReport;
import com.cloudcommerce.giftregistry.GiftRegistryResponse;
import com.cloudcommerce.giftregistry.SearchGiftRegistry;
import com.cloudcommerce.giftregistry.model.GiftRegistry;
import com.cloudcommerce.giftregistry.model.GiftRegistryItem;
import com.cloudcommerce.giftregistry.model.GiftRegistryPurchaseDetails;
import com.cloudcommerce.giftregistry.model.GiftRegistryUser;

public interface GiftDAO {
	public void addRegistryItem(GiftRegistryItem giftRegistryItem, Map<String, Object> inputMap,
			GiftRegistryResponse response);

	public void updateRegistryItem(GiftRegistryItem giftRegistryItem, Map<String, Object> inputMap,
			GiftRegistryResponse response);

	public void removeRegistryItem(Map<String, Object> inputMap, GiftRegistryResponse response);

	public void addGiftRegistry(GiftRegistry giftRegistry, Map<String, Object> inputMap, GiftRegistryResponse response)
			throws HibernateException, SQLException;

	public void updateRegistry(GiftRegistry giftRegistry, Map<String, Object> inputMap, GiftRegistryResponse response);

	public GiftRegistry getGiftRegistryById(Map<String, Object> inputMap, GiftRegistryResponse response);

	public void removeGiftRegistry(Map<String, Object> inputMap, GiftRegistryResponse response);

	public List searchGiftRegistry(SearchGiftRegistry searchRegistry, Map<String, Object> inputMap,
			GiftRegistryResponse response);

	public void indexSearchRecords(Map<String, Object> inputMap) throws Exception;

	public GiftRegistryUser getGiftRegistryUser(Map<String, Object> inputMap, GiftRegistryResponse response);

	public void updateGiftItemPurchaseDetails(Map<String, Object> inputMap, GiftRegistryResponse response);

	public void addPurchaseDetails(GiftRegistryPurchaseDetails purchaseDetails, Map<String, Object> inputMap,
			GiftRegistryResponse response);

	public void updateBasicItemDetails(GiftRegistryItem giftRegistryItem, Map<String, Object> inputMap,
			GiftRegistryResponse response);

	public List<GiftRegistryPurchaseReport> getPurchaseReport(Map<String, Object> inputMap, GiftRegistryResponse response);

	public GiftRegistryPurchaseDetails getPurchaseDetails(Map<String, Object> inputMap, GiftRegistryResponse response);
}
