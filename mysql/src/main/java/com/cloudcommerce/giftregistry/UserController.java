package com.cloudcommerce.giftregistry;


import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cloudcommerce.giftregistry.model.Andr_user;
import com.cloudcommerce.giftregistry.service.GiftRegistryService;

@Controller
public class UserController {

	private GiftRegistryService giftRegistryService;
	
	 	@Autowired(required=true)
	 	@Qualifier(value="giftRegistryService")
	    public void setGiftRegistryService(GiftRegistryService ps){
	        this.giftRegistryService = ps;
	    }
	 	 @RequestMapping(value = "/user", method = RequestMethod.GET)
	     public String listPersons(Model model) {
	         model.addAttribute("user", new Andr_user());
	        // model.addAttribute("listPersons", this.giftRegistryService.listUser());
	         return "user";
	     }
	      
}
