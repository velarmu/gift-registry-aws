package com.cloudcommerce.giftregistry;

import java.util.LinkedList;
import java.util.List;

public class GiftRegistryResponse {

	public GiftRegistryResponse() {
		super();
		errorMessages = new LinkedList<String>();
		resultObjects = new LinkedList<Object>();
	}

	private String result;

	private List<String> errorMessages;

	private List<Object> resultObjects;
	
	private String processedRegistryOrItemId;



	public String getProcessedRegistryOrItemId() {
		return processedRegistryOrItemId;
	}

	public void setProcessedRegistryOrItemId(String processedRegistryOrItemId) {
		this.processedRegistryOrItemId = processedRegistryOrItemId;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public List<String> getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public List<Object> getResultObjects() {
		return resultObjects;
	}

	public void setResultObjects(List<Object> resultObjects) {
		this.resultObjects = resultObjects;
	}

}
