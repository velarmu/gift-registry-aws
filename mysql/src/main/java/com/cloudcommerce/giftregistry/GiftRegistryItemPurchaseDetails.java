package com.cloudcommerce.giftregistry;

public class GiftRegistryItemPurchaseDetails {

	private static final String DELIMITER = ":";
	private String registryId;
	private int receivedQuantity;
	private String skuId;

	public String getRegistryId() {
		return registryId;
	}

	public void setRegistryId(String registryId) {
		this.registryId = registryId;
	}

	public int getReceivedQuantity() {
		return receivedQuantity;
	}

	public void setReceivedQuantity(int receivedQuantity) {
		this.receivedQuantity = receivedQuantity;
	}

	public String getSkuId() {
		return skuId;
	}

	public void setSkuId(String skuId) {
		this.skuId = skuId;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return registryId + DELIMITER + receivedQuantity + DELIMITER + skuId;
	}
}
