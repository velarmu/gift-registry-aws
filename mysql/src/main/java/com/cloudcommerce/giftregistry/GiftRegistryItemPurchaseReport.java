package com.cloudcommerce.giftregistry;

public class GiftRegistryItemPurchaseReport {
	
	
	
	public String getPurchaseDate() {
		return purchaseDate;
	}
	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	public String getSkuId() {
		return skuId;
	}
	public void setSkuId(String skuId) {
		this.skuId = skuId;
	}
	public String getItemName() {
		return ItemName;
	}
	public void setItemName(String itemName) {
		ItemName = itemName;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getStorePurchased() {
		return storePurchased;
	}
	public void setStorePurchased(String storePurchased) {
		this.storePurchased = storePurchased;
	}
	public String getPurchaserName() {
		return PurchaserName;
	}
	public void setPurchaserName(String purchaserName) {
		PurchaserName = purchaserName;
	}
	public String getTrackingNumber() {
		return trackingNumber;
	}
	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}
	public String getShippingAddress() {
		return shippingAddress;
	}
	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}
	private String purchaseDate;
	private String skuId;
	private String ItemName;
	private int quantity;
	private String storePurchased;
	private String PurchaserName;
	private String trackingNumber;
	private String shippingAddress;
	
	

}
