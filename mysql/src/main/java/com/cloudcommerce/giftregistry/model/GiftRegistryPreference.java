package com.cloudcommerce.giftregistry.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity(name = "GiftRegistryPreference")
@Table(name = "registry_gift_pref")
public class GiftRegistryPreference {

	@Id
	@GenericGenerator(parameters=@Parameter(name="sequence" , value="registry_gift_pref_seq#pref"), strategy="com.cloudcommerce.giftregistry.idgenerator.GiftRegistryIdGenerator", name = "registry_gift_pref_seq")
	@GeneratedValue(generator = "registry_gift_pref_seq")
	@Column(name = "interest_id")
	private String id;

	@Column(name = "interest_name")
	private String name;
	
	@Column(name = "interest_value")
	private String value;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
