package com.cloudcommerce.giftregistry.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonManagedReference;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.hibernate.search.annotations.Store;

import com.cloudcommerce.giftregistry.tools.GiftRegistryTools;

@Entity(name = "GiftRegistry")
@Table(name = "gift_registry_details")
@Indexed
@JsonIgnoreProperties(value = { "handler", "hibernateLazyInitializer" })
public class GiftRegistry {
	@Id
	@Column(name = "registry_id")
	// @GenericGenerator(parameters=@Parameter(name="sequence" ,
	// value="gift_registry_det_seq"),
	// strategy="com.cloudcommerce.giftregistry.idgenerator.GiftRegistryIdGenerator",
	// name = "gift_registry_det_seq")
	// @GeneratedValue(generator = "gift_registry_det_seq")
	@Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
	private String id;

	@Column(name = "registry_type")
	@Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
	private String type;

	@JsonManagedReference
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "gift_registry_item_rel", joinColumns = @JoinColumn(name = "registry_id") , inverseJoinColumns = @JoinColumn(name = "item_id") )
	private Set<GiftRegistryItem> giftRegistryItems;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(joinColumns = @JoinColumn(name = "registry_id") , name = "gift_reg_add_events_rel", inverseJoinColumns = @JoinColumn(name = "add_event_id") )
	private Set<GiftRegistryAdditionalEvent> additionalEvents;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "registry_gift_pref_rel", inverseJoinColumns = @JoinColumn(name = "interest_id") , joinColumns = @JoinColumn(name = "registry_id") )
	private Set<GiftRegistryPreference> giftPreferences;

	@JsonBackReference
	@ManyToOne
	@JoinTable(name = "user_gift_registry_rel", inverseJoinColumns = @JoinColumn(name = "user_id") , joinColumns = @JoinColumn(name = "registry_id") )
	private GiftRegistryUser giftRegistryUser;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "primary_registrant")
	@IndexedEmbedded
	private GiftRegistryRegistrant primaryRegistrant;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "co_registrant")
	@IndexedEmbedded
	private GiftRegistryRegistrant coRegistrant;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "pre_event_shipping")
	private GiftRegistryShipping preEventShipping;

	// @JsonManagedReference
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "gift_reg_prchse_details_rel", joinColumns = @JoinColumn(name = "registry_id") , inverseJoinColumns = @JoinColumn(name = "purchase_id") )
	private Set<GiftRegistryPurchaseDetails> purchaseDetails;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "event_id")
	@IndexedEmbedded
	private GiftRegistryEvent event;

	public Set<GiftRegistryPurchaseDetails> getPurchaseDetails() {
		return purchaseDetails;
	}

	public void setPurchaseDetails(Set<GiftRegistryPurchaseDetails> purchaseDetails) {
		this.purchaseDetails = purchaseDetails;
	}

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "post_event_shipping")
	private GiftRegistryShipping postEventShipping;

	@Column(name = "co_registrant_communication")
	private String coRegistrantCommunication;

	@Column(name = "slt_store_state")
	private String preferredState;

	@Column(name = "slt_store")
	private String preferredStore;

	@Column(name = "go_green_event")
	private String goGreenEvent;

	@Column(name = "guest_message")
	private String guestMessage;

	public GiftRegistryRegistrant getPrimaryRegistrant() {
		return primaryRegistrant;
	}

	public void setPrimaryRegistrant(GiftRegistryRegistrant primaryRegistrant) {
		this.primaryRegistrant = primaryRegistrant;
	}

	public GiftRegistryRegistrant getCoRegistrant() {
		return coRegistrant;
	}

	public void setCoRegistrant(GiftRegistryRegistrant coRegistrant) {
		this.coRegistrant = coRegistrant;
	}

	public GiftRegistryShipping getPreEventShipping() {
		return preEventShipping;
	}

	public void setPreEventShipping(GiftRegistryShipping preEventShipping) {
		this.preEventShipping = preEventShipping;
	}

	public GiftRegistryShipping getPostEventShipping() {
		return postEventShipping;
	}

	public void setPostEventShipping(GiftRegistryShipping postEventShipping) {
		this.postEventShipping = postEventShipping;
	}

	public String getCoRegistrantCommunication() {
		return GiftRegistryTools.returnEmptyIfNull(coRegistrantCommunication);
	}

	public void setCoRegistrantCommunication(String coRegistrantCommunication) {
		this.coRegistrantCommunication = coRegistrantCommunication;
	}

	public String getPreferredState() {
		return GiftRegistryTools.returnEmptyIfNull(preferredState);
	}

	public void setPreferredState(String preferredState) {
		this.preferredState = preferredState;
	}

	public String getPreferredStore() {
		return GiftRegistryTools.returnEmptyIfNull(preferredStore);
	}

	public void setPreferredStore(String preferredStore) {
		this.preferredStore = preferredStore;
	}

	public String getGoGreenEvent() {
		return GiftRegistryTools.returnEmptyIfNull(goGreenEvent);
	}

	public void setGoGreenEvent(String goGreenEvent) {
		this.goGreenEvent = goGreenEvent;
	}

	public String getGuestMessage() {
		return GiftRegistryTools.returnEmptyIfNull(guestMessage);
	}

	public void setGuestMessage(String guestMessage) {
		this.guestMessage = guestMessage;
	}

	public Set<GiftRegistryItem> getGiftRegistryItems() {
		return giftRegistryItems;
	}

	public void setGiftRegistryItems(Set<GiftRegistryItem> giftRegistryItems) {
		this.giftRegistryItems = giftRegistryItems;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Set<GiftRegistryAdditionalEvent> getAdditionalEvents() {
		return additionalEvents;
	}

	public void setAdditionalEvents(Set<GiftRegistryAdditionalEvent> additionalEvents) {
		this.additionalEvents = additionalEvents;
	}

	public Set<GiftRegistryPreference> getGiftPreferences() {
		return giftPreferences;
	}

	public void setGiftPreferences(Set<GiftRegistryPreference> giftPreferences) {
		this.giftPreferences = giftPreferences;
	}

	public GiftRegistryEvent getEvent() {
		return event;
	}

	public void setEvent(GiftRegistryEvent event) {
		this.event = event;
	}

	public GiftRegistryUser getGiftRegistryUser() {
		return giftRegistryUser;
	}

	public void setGiftRegistryUser(GiftRegistryUser giftRegistryUser) {
		this.giftRegistryUser = giftRegistryUser;
	}

	@Override
	public String toString() {
		return "id=" + id + ", type=" + type;
	}
}
