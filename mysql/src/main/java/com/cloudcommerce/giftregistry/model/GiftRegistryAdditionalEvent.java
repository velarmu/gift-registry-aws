package com.cloudcommerce.giftregistry.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity(name = "GiftRegistryAdditionalEvent")
@Table(name = "gift_additional_events")
public class GiftRegistryAdditionalEvent {

	@Id
	@GenericGenerator(parameters=@Parameter(name="sequence" , value="gift_additional_events_seq"), strategy="com.cloudcommerce.giftregistry.idgenerator.GiftRegistryIdGenerator", name = "gift_additional_events_seq")
	@GeneratedValue(generator = "gift_additional_events_seq")
	@Column(name = "add_event_id")
	private String id;

	@Column(name = "add_event_name")
	private String name;
	
	@Column(name = "add_event_time")
	private String time;
	
	@Column(name = "add_event_date")
	private String date;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

}
