package com.cloudcommerce.giftregistry.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Store;

import com.cloudcommerce.giftregistry.tools.GiftRegistryTools;

@Entity(name = "GiftRegistryRegistrant")
@Table(name = "gift_registrant_details")
public class GiftRegistryRegistrant {

	@Id
	@GenericGenerator(parameters=@Parameter(name="sequence" , value="gift_registrant_details_seq"), strategy="com.cloudcommerce.giftregistry.idgenerator.GiftRegistryIdGenerator", name = "gift_registrant_details_seq")
	@GeneratedValue(generator = "gift_registrant_details_seq")
	@Column(name = "registrant_id")
	private String id;

	@Column(name = "title")
	private String title;

	@Column(name = "first_name")
	@Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
	private String firstName;

	@Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
	@Column(name = "last_name")
	private String lastName;

	@Column(name = "future_last_name")
	private String futureLastName;

	@Column(name = "email")
	private String email;

	@Column(name = "evening_phone_number")
	private String eveningPhone;

	@Column(name = "phone_number")
	private String phone;
	
	@Column(name = "registrant_type")
	private String type;

	public String getId() {
		return GiftRegistryTools.returnEmptyIfNull(id);
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return GiftRegistryTools.returnEmptyIfNull(title);
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return GiftRegistryTools.returnEmptyIfNull(firstName);
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return GiftRegistryTools.returnEmptyIfNull(lastName);
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFutureLastName() {
		return GiftRegistryTools.returnEmptyIfNull(futureLastName);
	}

	public void setFutureLastName(String futureLastName) {
		this.futureLastName = futureLastName;
	}

	public String getEmail() {
		return GiftRegistryTools.returnEmptyIfNull(email);
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEveningPhone() {
		return GiftRegistryTools.returnEmptyIfNull(eveningPhone);
	}

	public void setEveningPhone(String eveningPhone) {
		this.eveningPhone = eveningPhone;
	}

	public String getPhone() {
		return GiftRegistryTools.returnEmptyIfNull(phone);
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getType() {
		return GiftRegistryTools.returnEmptyIfNull(type);
	}

	public void setType(String type) {
		this.type = type;
	}

}
