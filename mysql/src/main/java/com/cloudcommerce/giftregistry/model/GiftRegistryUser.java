package com.cloudcommerce.giftregistry.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonManagedReference;

@Entity(name = "GiftRegistryUser")
@Table(name = "user_details")
public class GiftRegistryUser {
	@Id
	@Column(name = "user_id")
	private String id;

	@Column(name = "user_name")
	private String name;

	@JsonManagedReference
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "user_gift_registry_rel", joinColumns = @JoinColumn(name = "user_id") , inverseJoinColumns = @JoinColumn(name = "registry_id") )
	private Set<GiftRegistry> giftRegistries;

	@Override
	public String toString() {
		return "id=" + id + ", name=" + name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<GiftRegistry> getGiftRegistries() {
		return giftRegistries;
	}

	public void setGiftRegistries(Set<GiftRegistry> giftRegistries) {
		this.giftRegistries = giftRegistries;
	}
}
