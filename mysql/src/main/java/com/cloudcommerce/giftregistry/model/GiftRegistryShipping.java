package com.cloudcommerce.giftregistry.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.cloudcommerce.giftregistry.tools.GiftRegistryTools;

@Entity(name = "GiftRegistryShipping")
@Table(name = "gift_shipping_details")
public class GiftRegistryShipping {

	@Id
	@GenericGenerator(parameters=@Parameter(name="sequence" , value="gift_shipping_details_seq"), strategy="com.cloudcommerce.giftregistry.idgenerator.GiftRegistryIdGenerator", name = "gift_shipping_details_seq")
	@GeneratedValue(generator = "gift_shipping_details_seq")
	@Column(name = "shipping_id")
	private String id;

	@Column(name = "shipping_type")
	private String type;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "city")
	private String city;

	@Column(name = "state")
	private String state;

	@Column(name = "address1")
	private String address1;

	@Column(name = "address2")
	private String address2;

	@Column(name = "postal_code")
	private String zip;

	@Column(name = "phone_number")
	private String phone;

	public String getId() {
		return GiftRegistryTools.returnEmptyIfNull(id);
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return GiftRegistryTools.returnEmptyIfNull(type);
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFirstName() {
		return GiftRegistryTools.returnEmptyIfNull(firstName);
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return GiftRegistryTools.returnEmptyIfNull(lastName);
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCity() {
		return GiftRegistryTools.returnEmptyIfNull(city);
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return GiftRegistryTools.returnEmptyIfNull(state);
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getAddress1() {
		return GiftRegistryTools.returnEmptyIfNull(address1);
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return GiftRegistryTools.returnEmptyIfNull(address2);
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getZip() {
		return GiftRegistryTools.returnEmptyIfNull(zip);
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getPhone() {
		return GiftRegistryTools.returnEmptyIfNull(phone);
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

}
