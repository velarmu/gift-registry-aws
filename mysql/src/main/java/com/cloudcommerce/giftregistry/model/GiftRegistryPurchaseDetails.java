package com.cloudcommerce.giftregistry.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;



@Entity(name = "GiftRegistryPurchaseDetails")
@Table(name = "gift_reg_purchase_details")
public class GiftRegistryPurchaseDetails {
	

	
	@Id
	@GenericGenerator(parameters=@Parameter(name="sequence" , value="gift_reg_purchase_details_seq"), strategy="com.cloudcommerce.giftregistry.idgenerator.GiftRegistryIdGenerator", name = "gift_reg_purchase_details_seq")
	@GeneratedValue(generator = "gift_reg_purchase_details_seq")
	@Column(name = "purchase_id")
	private String id;
	
	@Column(name = "first_name")
	private String firstName;

	
	@Column(name = "last_name")
	private String lastName;

	@Column(name = "sku_id")
	private String skuId;

	@Column(name = "store_id")
	private String storeId;

	@Column(name = "sale_price")
	private Double salePrice;
	
	@Column(name = "item_Name")
	private String itemName;
	
	@Column(name = "quantity")
	private int quantity;
	
	@Column(name = "purchase_date")
	private String purchaseDate;
	
	@Column(name = "tracking_number")
	private String trackingNumber;
	
	
	
	
	public String getTrackingNumber() {
		return trackingNumber;
	}

	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	@JsonBackReference
	@ManyToOne
	@JoinTable(name = "gift_reg_prchse_details_rel", inverseJoinColumns = @JoinColumn(name = "registry_id") , joinColumns = @JoinColumn(name = "purchase_id") )
	private GiftRegistry giftRegistry;

	public GiftRegistry getGiftRegistry() {
		return giftRegistry;
	}

	public void setGiftRegistry(GiftRegistry giftRegistry) {
		this.giftRegistry = giftRegistry;
	}



	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSkuId() {
		return skuId;
	}

	public void setSkuId(String skuId) {
		this.skuId = skuId;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public Double getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(Double salePrice) {
		this.salePrice = salePrice;
	}


	
	}
