package com.cloudcommerce.giftregistry.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity(name = "GiftItem")
@Table(name = "gift_registry_item")
@JsonIgnoreProperties(value = { "handler", "hibernateLazyInitializer" })
public class GiftRegistryItem {


	@Id
	@GenericGenerator(parameters=@Parameter(name="sequence" , value="gift_registry_item_seq"), strategy="com.cloudcommerce.giftregistry.idgenerator.GiftRegistryIdGenerator", name = "gift_registry_item_seq")
	@GeneratedValue(generator = "gift_registry_item_seq")
	@Column(name = "item_id")
	private String id;

	@Column(name = "requested_quantity")
	private int requestedQuantity;

	@Column(name = "received_quantity")
	private int receivedQuantity;

	@Column(name = "category")
	private String category;

	@Column(name = "product_id")
	private String productId;
	
	@Column(name = "sku_id")
	private String skuId;
	
	@Column(name = "display_name")
	private String displayName;
	
	@Column(name = "image_url")
	private String imageURL;


	@Column(name = "product_url")
	private String productURL;


	@Column(name = "list_price")
	private Double listPrice;


	@Column(name = "favorite_message")
	private String favoriteMessage;
	
	public String getFavoriteMessage() {
		return favoriteMessage;
	}

	public void setFavoriteMessage(String favoriteMessage) {
		this.favoriteMessage = favoriteMessage;
	}
	
	@Column(name = "favorite")
	private String favorite;

	public String getFavorite() {
		return favorite;
	}

	public void setFavorite(String favorite) {
		this.favorite = favorite;
	}


	
	@Column(name = "sale_price")
	private Double salePrice;
	

	@JsonBackReference
	@ManyToOne
	@JoinTable(name = "gift_registry_item_rel", inverseJoinColumns = @JoinColumn(name = "registry_id") , joinColumns = @JoinColumn(name = "item_id") )
	private GiftRegistry giftRegistry;

	public GiftRegistry getGiftRegistry() {
		return giftRegistry;
	}

	public void setGiftRegistry(GiftRegistry giftRegistry) {
		this.giftRegistry = giftRegistry;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}



	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getRequestedQuantity() {
		return requestedQuantity;
	}

	public void setRequestedQuantity(int requestedQuantity) {
		this.requestedQuantity = requestedQuantity;
	}

	public int getReceivedQuantity() {
		return receivedQuantity;
	}

	public void setReceivedQuantity(int receivedQuantity) {
		this.receivedQuantity = receivedQuantity;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return "id=" + id + ", name=" + requestedQuantity + "receivedQuantity=" + receivedQuantity + "category name="
				+ category;
	}

	public String getSkuId() {
		return skuId;
	}

	public void setSkuId(String skuId) {
		this.skuId = skuId;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public String getProductURL() {
		return productURL;
	}

	public void setProductURL(String productURL) {
		this.productURL = productURL;
	}

	public Double getListPrice() {
		return listPrice;
	}

	public void setListPrice(Double listPrice) {
		this.listPrice = listPrice;
	}

	public Double getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(Double salePrice) {
		this.salePrice = salePrice;
	}

}
