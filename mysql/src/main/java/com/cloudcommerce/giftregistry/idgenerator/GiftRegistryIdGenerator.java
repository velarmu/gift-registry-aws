package com.cloudcommerce.giftregistry.idgenerator;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.SequenceGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cloudcommerce.giftregistry.Constants;
import com.cloudcommerce.giftregistry.tools.GiftRegistryTools;

public class GiftRegistryIdGenerator extends SequenceGenerator {

	private static final Logger logger = LoggerFactory.getLogger(GiftRegistryIdGenerator.class);

	public Serializable generate(SessionImplementor session, Object object) throws HibernateException {
		String sequenceName = super.getSequenceName();
		String generatedId = null;
		logger.info("sequencename=" + sequenceName);
		String[] sequenceArray = !GiftRegistryTools.isBlank(sequenceName)
				? sequenceName.split(Constants.SEQUENCE_NAME_SEPARATOR) : null;
		if (sequenceArray != null && sequenceArray.length > 0) {
			sequenceName = sequenceArray[0];
			String prefix = sequenceArray.length > 1 ? sequenceArray[1] : Constants.EMPTY_STRING;
			try {
				generatedId = GiftRegistryTools.getGeneratedId(sequenceName, session.connection(), prefix);
			} catch (SQLException e) {
				e.printStackTrace();
				throw new HibernateException("Unable to generate GiftRegistryIdGenerator sequence");
			}
		}
		return generatedId;
	}
}