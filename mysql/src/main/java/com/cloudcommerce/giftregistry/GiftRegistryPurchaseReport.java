package com.cloudcommerce.giftregistry;

import java.util.ArrayList;
import java.util.List;

public class GiftRegistryPurchaseReport {
	
	
	public GiftRegistryPurchaseReport() {
	
		itemReports = new ArrayList<GiftRegistryItemPurchaseReport>();
	
	}
	private List<GiftRegistryItemPurchaseReport> itemReports;
	private String registryId;
	private String registryDate;
	public List<GiftRegistryItemPurchaseReport> getItemReports() {
		return itemReports;
	}
	public void setItemReports(List<GiftRegistryItemPurchaseReport> itemReports) {
		this.itemReports = itemReports;
	}
	public String getRegistryId() {
		return registryId;
	}
	public void setRegistryId(String registryId) {
		this.registryId = registryId;
	}
	public String getRegistryDate() {
		return registryDate;
	}
	public void setRegistryDate(String registryDate) {
		this.registryDate = registryDate;
	}

}
