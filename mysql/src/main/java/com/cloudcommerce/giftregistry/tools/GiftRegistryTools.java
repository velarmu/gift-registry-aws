package com.cloudcommerce.giftregistry.tools;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cloudcommerce.giftregistry.Constants;
import com.cloudcommerce.giftregistry.SearchGiftRegistry;
import com.cloudcommerce.giftregistry.dao.GiftDAOImpl;
import com.cloudcommerce.giftregistry.model.GiftRegistry;
import com.cloudcommerce.giftregistry.model.GiftRegistryAdditionalEvent;
import com.cloudcommerce.giftregistry.model.GiftRegistryEvent;
import com.cloudcommerce.giftregistry.model.GiftRegistryItem;
import com.cloudcommerce.giftregistry.model.GiftRegistryRegistrant;
import com.cloudcommerce.giftregistry.model.GiftRegistryShipping;

public class GiftRegistryTools {

	private static final Logger logger = LoggerFactory.getLogger(GiftRegistryTools.class);

	public static void validateRegistryObject(List<String> errorList, GiftRegistry registryObject) {
		if (registryObject != null) {
			validateEmptyString("Please enter valid registry type", registryObject.getType(), errorList);
			validateEventObject(registryObject.getEvent(), errorList);
			validateCoRegistrantObject(registryObject.getCoRegistrant(), errorList);
			validatePrimaryRegistrantObject(registryObject.getPrimaryRegistrant(), errorList);
			validatePreEventShippingObject(registryObject.getPreEventShipping(), errorList);
			// validatePostEventShippingObject(registryObject.getPostEventShipping(),
			// errorList);
			if (registryObject.getAdditionalEvents() != null && registryObject.getAdditionalEvents().size() > 0) {
				for (GiftRegistryAdditionalEvent additionalEvent : registryObject.getAdditionalEvents()) {
					validateAdditionalEventObject(additionalEvent, errorList);
				}
			}
		} else {
			errorList.add("Registry details are missing");
		}
	}

	public static void validateSearchRegistryObject(SearchGiftRegistry searchRegistryObject, List<String> errorList) {
		if (searchRegistryObject != null) {
			if ((isBlank(searchRegistryObject.getRegistryId())) && (isBlank(searchRegistryObject.getLastName()))) {
				errorList.add("Registry No. or Last Name is required");
			} else {
				if (!isBlank(searchRegistryObject.getLastName())) {
					validateSearchName("Lastname should be more than 1 character", searchRegistryObject.getLastName(),
							errorList);
				}
				if (!isBlank(searchRegistryObject.getFirstName())) {
					validateSearchName("Firstname should be more than 1 character", searchRegistryObject.getFirstName(),
							errorList);
				}
				if (searchRegistryObject.getEventMonth() != 0) {
					validateSearchRegistryMonth("Please provide valid event month",
							searchRegistryObject.getEventMonth(), errorList);
				}

			}
		} else {
			errorList.add("Please provide valid search details");
		}
	}

	public static void validateEventObject(GiftRegistryEvent eventObject, List<String> errorList) {
		if (eventObject != null) {
			Date parsedDate = validateDateString("Please enter valid event date", eventObject.getEventDate(),
					errorList);
			if (parsedDate != null && parsedDate instanceof Date) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(parsedDate);
				int month = cal.get(Calendar.MONTH) + 1;
				logger.info("parseddate=" + parsedDate.toString());
				logger.info("month to be updated in validateEventObject-" + String.valueOf(month));
				eventObject.setEventMonth(month);
			}
			validateTimeString("Please enter valid event time", eventObject.getEventTime(), errorList);
			validateEmptyString("Please provide valid Publish State", eventObject.getPublishState(), errorList);
		} else {
			errorList.add("Event details are missing");
		}
	}

	public static void validateAdditionalEventObject(GiftRegistryAdditionalEvent eventObject, List<String> errorList) {
		if (eventObject != null) {
			if (!isBlank(eventObject.getDate())) {
				validateDateString("Please enter valid additional event date for event " + eventObject.getName(),
						eventObject.getDate(), errorList);
			}
			if (!isBlank(eventObject.getTime())) {
				validateTimeString("Please enter valid additional event time for event " + eventObject.getName(),
						eventObject.getTime(), errorList);
			}
		}
	}

	public static void validatePreEventShippingObject(GiftRegistryShipping shippingObject, List<String> errorList) {
		if (shippingObject != null) {
			validateEmptyString("Please enter valid shipping first name", shippingObject.getFirstName(), errorList);
			validateEmptyString("Please enter valid shipping last name", shippingObject.getLastName(), errorList);
			validateEmptyString("Please enter valid shipping address1", shippingObject.getAddress1(), errorList);
			// validatePhoneNumber("Please enter valid shipping phone number",
			// shippingObject.getPhone(), errorList);
			validateEmptyString("Please enter valid shipping state", shippingObject.getState(), errorList);
			validateEmptyString("Please enter valid shipping city", shippingObject.getCity(), errorList);
			validateEmptyString("Please enter valid shipping postal code", shippingObject.getZip(), errorList);
		} else {
			errorList.add("shipping details are missing");
		}
	}

	public static void validatePostEventShippingObject(GiftRegistryShipping shippingObject, List<String> errorList) {
		if (shippingObject != null) {
			validateEmptyString("Please enter valid post event shipping first name", shippingObject.getFirstName(),
					errorList);
			validateEmptyString("Please enter valid post event shipping last name", shippingObject.getLastName(),
					errorList);
			validateEmptyString("Please enter valid post event shipping address1", shippingObject.getAddress1(),
					errorList);
			// validatePhoneNumber("Please enter valid post event shipping phone
			// number", shippingObject.getPhone(), errorList);
			validateEmptyString("Please enter valid post event shipping state", shippingObject.getState(), errorList);
			validateEmptyString("Please enter valid post event shipping city", shippingObject.getCity(), errorList);
			validateEmptyString("Please enter valid post event shipping postal code", shippingObject.getZip(),
					errorList);
		}
	}

	public static void validatePrimaryRegistrantObject(GiftRegistryRegistrant registrantObject,
			List<String> errorList) {
		if (registrantObject != null) {
			validateEmptyString("Please enter valid registrant first name", registrantObject.getFirstName(), errorList);
			validateEmptyString("Please enter valid registrant last name", registrantObject.getLastName(), errorList);
			validatePhoneNumber("Please enter valid phone number", registrantObject.getPhone(), errorList);
			if (!isBlank(registrantObject.getEveningPhone())) {
				validatePhoneNumber("Please enter valid evening phone number", registrantObject.getEveningPhone(),
						errorList);
			}
		} else {
			errorList.add("Registrant details are missing");
		}
	}

	public static void validateCoRegistrantObject(GiftRegistryRegistrant registrantObject, List<String> errorList) {
		if (registrantObject != null) {
			// validateEmptyString("Please enter valid registrant first name",
			// registrantObject.getFirstName(), errorList);
			// validateEmptyString("Please enter valid registrant last name",
			// registrantObject.getLastName(), errorList);
			if (!isBlank(registrantObject.getPhone())) {
				validatePhoneNumber("Please enter valid co registrant phone number", registrantObject.getPhone(),
						errorList);
			}
			if (!isBlank(registrantObject.getEveningPhone())) {
				validatePhoneNumber("Please enter valid co registrant evening phone number",
						registrantObject.getEveningPhone(), errorList);
			}
			if (!isBlank(registrantObject.getEmail())) {
				validateEmail("Please enter valid email", registrantObject.getEmail(), errorList);
			}
		}

	}

	public static void validateRegistryForItemProcess(GiftRegistry registryObject, List<String> errorList) {
		if (registryObject != null) {
			if (registryObject.getGiftRegistryItems() != null && registryObject.getGiftRegistryItems().size() > 0) {
				for (GiftRegistryItem registryItemObject : registryObject.getGiftRegistryItems()) {
					validateRegistryItemObject(errorList, registryItemObject);
				}
			} else {
				errorList.add("Registry Item details are missing");
			}
		} else {
			errorList.add("Registry details are missing");
		}
	}

	public static void validateRegistryItemObject(List<String> errorList, GiftRegistryItem registryItemObject) {
		validateEmptyString("Please provide valid product Id", registryItemObject.getProductId(), errorList);
		validateQuantity("Please provide requested quantity greater than zero",
				registryItemObject.getRequestedQuantity(), errorList);
	}

	public static void validateQuantity(String errorString, int quantity, List<String> errorList) {
		if (quantity <= 0) {
			errorList.add(errorString);
		}
	}

	public static void validateEmptyString(String errorString, String inputString, List<String> errorList) {
		if (isBlank(inputString)) {
			errorList.add(errorString);
		}
	}

	public static Date validateDateString(String errorString, String inputDate, List<String> errorList) {
		if (isBlank(inputDate)) {
			errorList.add(errorString);
		} else {
			DateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT);
			format.setLenient(false);
			Date parsedDate;
			try {
				parsedDate = format.parse(inputDate);
				logger.info("parseddate=" + parsedDate);
				return parsedDate;
			} catch (ParseException e) {

				e.printStackTrace();
				errorList.add(errorString);
			}

		}
		return null;
	}

	public static void validateTimeString(String errorString, String inputTime, List<String> errorList) {
		if (!isBlank(inputTime)) {
			SimpleDateFormat format = new SimpleDateFormat(Constants.TIME_FORMAT);
			try {
				format.setLenient(false);
				format.parse(inputTime);
			} catch (ParseException e) {
				e.printStackTrace();
				errorList.add(errorString);
			}
		}
	}

	public static void validateEmail(String errorString, String email, List<String> errorList) {
		if (isBlank(email) || !email.matches(Constants.EMAIL_REGEX)) {
			errorList.add(errorString);
		}
	}

	public static void validatePhoneNumber(String errorString, String phoneNumber, List<String> errorList) {
		if (isBlank(phoneNumber) || !phoneNumber.replace(Constants.PHONE_NUMBER_SEPARATOR, Constants.EMPTY_STRING)
				.matches(Constants.PHONE_NUMBER_REGEX)) {
			errorList.add(errorString);
		}
	}

	public static void validateSearchName(String errorString, String name, List<String> errorList) {
		if (isBlank(name) || (name.trim().length() < 2)) {
			errorList.add(errorString);
		}
	}

	public static void validateSearchRegistryMonth(String errorString, int eventMonth, List<String> errorList) {
		if (eventMonth == 0 || eventMonth > 12) {
			errorList.add(errorString);
		}
	}

	public static String getGeneratedId(String sequenceName, Connection connection, String prefix) throws SQLException {
		String code = null;
		String query = "call increment_get_seq_val(?,?);";
		logger.info("query=" + query);
		CallableStatement cStmt = connection.prepareCall(query);
		cStmt.setString(1, sequenceName);
		cStmt.registerOutParameter(2, java.sql.Types.INTEGER);
		cStmt.executeUpdate();
		int id = cStmt.getInt(2);
		code = prefix + id;
		logger.info("sequence generated=" + code);
		return code;
	}

	public static boolean isBlank(String input) {
		return input == null || input.trim().isEmpty();
	}

	public static String returnEmptyIfNull(String input) {
		return input == null ? Constants.EMPTY_STRING : input;
	}
}
