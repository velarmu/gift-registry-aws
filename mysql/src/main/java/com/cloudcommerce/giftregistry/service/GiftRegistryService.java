package com.cloudcommerce.giftregistry.service;

import java.util.List;
import java.util.Map;

import com.cloudcommerce.giftregistry.GiftRegistryPurchaseReport;
import com.cloudcommerce.giftregistry.GiftRegistryResponse;
import com.cloudcommerce.giftregistry.SearchGiftRegistry;
import com.cloudcommerce.giftregistry.model.GiftRegistry;
import com.cloudcommerce.giftregistry.model.GiftRegistryItem;
import com.cloudcommerce.giftregistry.model.GiftRegistryPurchaseDetails;

public interface GiftRegistryService {
	public void addRegistryItem(GiftRegistryItem giftRegistryItem, Map<String, Object> inputMap,
			GiftRegistryResponse response);

	public void updateRegistryItem(GiftRegistryItem giftRegistryItem, Map<String, Object> inputMap,
			GiftRegistryResponse response);

	public void removeRegistryItem(Map<String, Object> inputMap, GiftRegistryResponse response);

	public void addRegistry(GiftRegistry giftRegistry, Map<String, Object> inputMap, GiftRegistryResponse response);

	public void updateRegistry(GiftRegistry giftRegistry, Map<String, Object> inputMap, GiftRegistryResponse response);
	
	public void updateRegistryWithItem(GiftRegistry giftRegistry, Map<String, Object> inputMap, GiftRegistryResponse response);

    public void getGiftRegistryById(Map<String, Object> inputMap, GiftRegistryResponse response);

	public void removeGiftRegistry(Map<String, Object> inputMap, GiftRegistryResponse response);

	public void searchGiftRegistry(SearchGiftRegistry searchRegistry, Map<String, Object> inputMap,
			GiftRegistryResponse response);

	public void indexSearchRecords(Map<String, Object> inputMap, GiftRegistryResponse response);

	public void getGiftRegistriesByUserId(Map<String, Object> inputMap, GiftRegistryResponse response);

	public void updateGiftItemPurchaseDetails(Map<String, Object> inputMap, GiftRegistryResponse response);

	public void getBasicGiftRegistriesByUserId(Map<String, Object> inputMap, GiftRegistryResponse response);
	  
	public void addPurchaseDetails(GiftRegistryPurchaseDetails purchaseDetails,Map<String, Object> inputMap, GiftRegistryResponse response);
	
     public void updateBasicItemDetails(GiftRegistryItem giftRegistryItem,Map<String, Object> inputMap, GiftRegistryResponse response);

	public void getPurchaseReport(Map<String, Object> inputMap, GiftRegistryResponse response);
	
	
		
}
