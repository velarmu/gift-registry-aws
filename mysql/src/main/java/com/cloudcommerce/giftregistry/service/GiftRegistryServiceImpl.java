package com.cloudcommerce.giftregistry.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.cloudcommerce.giftregistry.BasicRegistryDetails;
import com.cloudcommerce.giftregistry.Constants;
import com.cloudcommerce.giftregistry.GiftRegistryItemPurchaseDetails;
import com.cloudcommerce.giftregistry.GiftRegistryPurchaseReport;
import com.cloudcommerce.giftregistry.GiftRegistryResponse;
import com.cloudcommerce.giftregistry.SearchGiftRegistry;
import com.cloudcommerce.giftregistry.dao.GiftDAO;
import com.cloudcommerce.giftregistry.model.GiftRegistry;
import com.cloudcommerce.giftregistry.model.GiftRegistryEvent;
import com.cloudcommerce.giftregistry.model.GiftRegistryItem;
import com.cloudcommerce.giftregistry.model.GiftRegistryPurchaseDetails;
import com.cloudcommerce.giftregistry.model.GiftRegistryRegistrant;
import com.cloudcommerce.giftregistry.model.GiftRegistryShipping;
import com.cloudcommerce.giftregistry.model.GiftRegistryUser;
import com.cloudcommerce.giftregistry.tools.GiftRegistryTools;

@Service
public class GiftRegistryServiceImpl implements GiftRegistryService {

	private static final Logger logger = LoggerFactory.getLogger(GiftRegistryServiceImpl.class);

	private GiftDAO giftDAO;

	public void setGiftDAO(GiftDAO giftDAO) {
		this.giftDAO = giftDAO;
	}

	@Override
	public void addRegistryItem(GiftRegistryItem giftRegistryItem, Map<String, Object> inputMap,
			GiftRegistryResponse response) {
		GiftRegistryTools.validateRegistryItemObject(response.getErrorMessages(), giftRegistryItem);
		if (response.getErrorMessages().size() <= 0) {
			try {
				this.giftDAO.addRegistryItem(giftRegistryItem, inputMap, response);
			} catch (Exception ex) {
				logger.error("Exception during addRegistryItem");
				ex.printStackTrace();
				response.getErrorMessages().add("System error. Please try again");
			}
		}
		setResultInResponseObject(response);
	}

	@Override
	public void addRegistry(GiftRegistry giftRegistry, Map<String, Object> inputMap, GiftRegistryResponse response) {
		GiftRegistryTools.validateRegistryObject(response.getErrorMessages(), giftRegistry);
		if (response.getErrorMessages().size() <= 0) {
			try {
				this.giftDAO.addGiftRegistry(giftRegistry, inputMap, response);
			} catch (Exception ex) {
				logger.error("Exception during addRegistry");
				ex.printStackTrace();
				response.getErrorMessages().add("System error. Please try again");
			}
		}
		setResultInResponseObject(response);
	}

	@Override
	public void updateRegistry(GiftRegistry giftRegistry, Map<String, Object> inputMap, GiftRegistryResponse response) {
		GiftRegistryTools.validateRegistryObject(response.getErrorMessages(), giftRegistry);
		if (response.getErrorMessages().size() <= 0 ) {
			try {
				         ArrayList<GiftRegistryItem> giftRegistryItem = new  ArrayList<GiftRegistryItem>();
		               	if (response.getErrorMessages().size() <= 0 && giftRegistry.getGiftRegistryItems() != null && giftRegistry.getGiftRegistryItems().size() > 0 ) 
		               	{
							
							 
	     					 for(GiftRegistryItem item:giftRegistry.getGiftRegistryItems())
							{
							 
							  giftRegistryItem.add(item);
							
							}
							 giftRegistry.getGiftRegistryItems().removeAll(giftRegistryItem) ;
							 
						}
							 this.giftDAO.updateRegistry(giftRegistry, inputMap, response);
							 inputMap.put(Constants.REGISTRY_ID, giftRegistry.getId());
							 if (giftRegistryItem != null &&  giftRegistryItem.size() > 0  ) 
							 {
						      for(GiftRegistryItem item:giftRegistryItem)
							{	
						    	  this.giftDAO.updateRegistryItem(item, inputMap, response);		
						    }
							 }
							 
						
						} 
						
						catch (Exception ex) {
							logger.error("Exception during updateRegistryWithItem");
							ex.printStackTrace();
							response.getErrorMessages().add("System error. Please try again");
						}
		        }
			
						setResultInResponseObject(response);
					}
				


	@Override
	public void getGiftRegistryById(Map<String, Object> inputMap, GiftRegistryResponse response) {
		try {
			GiftRegistry giftRegistry = this.giftDAO.getGiftRegistryById(inputMap, response);
			validateEmptyObjects(giftRegistry);
			response.getResultObjects().add(giftRegistry);
		} catch (Exception ex) {
			logger.error("Exception during getGiftRegistryById");
			ex.printStackTrace();
			response.getErrorMessages().add("System error. Please try again");
		}
		setResultInResponseObject(response);
	}

	@Override
	public void removeGiftRegistry(Map<String, Object> inputMap, GiftRegistryResponse response) {
		try {
			this.giftDAO.removeGiftRegistry(inputMap, response);
		} catch (Exception ex) {
			logger.error("Exception during removeGiftRegistry");
			ex.printStackTrace();
			response.getErrorMessages().add("System error. Please try again");
		}
		setResultInResponseObject(response);
	}

	@Override
	public void updateRegistryItem(GiftRegistryItem giftRegistryItem, Map<String, Object> inputMap,
			GiftRegistryResponse response) {
		GiftRegistryTools.validateRegistryItemObject(response.getErrorMessages(), giftRegistryItem);
		if (response.getErrorMessages().size() <= 0) {
			try {
				this.giftDAO.updateRegistryItem(giftRegistryItem, inputMap, response);
			} catch (Exception ex) {
				logger.error("Exception during updateRegistryItem");
				ex.printStackTrace();
				response.getErrorMessages().add("System error. Please try again");
			}
		}
		setResultInResponseObject(response);
	}
	
	

	@Override
	public void removeRegistryItem(Map<String, Object> inputMap, GiftRegistryResponse response) {
		try {
			this.giftDAO.removeRegistryItem(inputMap, response);
		} catch (Exception ex) {
			logger.error("Exception during removeRegistryItem");
			ex.printStackTrace();
			response.getErrorMessages().add("System error. Please try again");
		}
		setResultInResponseObject(response);
	}

	@Override
	public void searchGiftRegistry(SearchGiftRegistry searchRegistry, Map<String, Object> inputMap,
			GiftRegistryResponse response) {
		GiftRegistryTools.validateSearchRegistryObject(searchRegistry, response.getErrorMessages());
		if (response.getErrorMessages().size() <= 0) {
			try {
				List giftRegistries = this.giftDAO.searchGiftRegistry(searchRegistry, inputMap, response);
				response.getResultObjects().addAll(giftRegistries);
			} catch (Exception ex) {
				logger.error("Exception during searchGiftRegistry");
				ex.printStackTrace();
				response.getErrorMessages().add("System error. Please try again");
			}
		}
		setResultInResponseObject(response);
	}

	@Override
	public void indexSearchRecords(Map<String, Object> inputMap, GiftRegistryResponse response) {
		try {
			this.giftDAO.indexSearchRecords(inputMap);
		} catch (Exception ex) {
			logger.error("Exception during indexSearchRecords");
			ex.printStackTrace();
			response.getErrorMessages().add("System error. Please try again");
		}
		setResultInResponseObject(response);

	}

	@Override
	public void getGiftRegistriesByUserId(Map<String, Object> inputMap, GiftRegistryResponse response) {
		try {
			GiftRegistryUser giftRegistryUser = this.giftDAO.getGiftRegistryUser(inputMap, response);
			if (giftRegistryUser != null && response.getErrorMessages().size() <= 0) {
				for (GiftRegistry giftRegistry : giftRegistryUser.getGiftRegistries()) {
					validateEmptyObjects(giftRegistry);
					response.getResultObjects().add(giftRegistry);
				}
			}
		} catch (Exception ex) {
			logger.error("Exception during getGiftRegistriesByUserId");
			ex.printStackTrace();
			response.getErrorMessages().add("System error. Please try again");
		}
		setResultInResponseObject(response);
	}
	
	
    public void updateBasicItemDetails(GiftRegistryItem giftRegistryItem,Map<String, Object> inputMap, GiftRegistryResponse response){
		try {
			this.giftDAO.updateBasicItemDetails(giftRegistryItem,inputMap,response);
		} catch (Exception ex) {
			logger.error("updateBasicItemDetails");
			ex.printStackTrace();
			response.getErrorMessages().add("System error. Please try again");
		}
		setResultInResponseObject(response);

	}

	private void setResultInResponseObject(GiftRegistryResponse response) {
		if (response.getErrorMessages().size() <= 0) {
			response.setResult(Constants.SUCCESS);
		} else {
			response.setResult(Constants.FAILURE);
		}
	}

	@Override
	public void updateGiftItemPurchaseDetails(Map<String, Object> inputMap, GiftRegistryResponse response) {
		String orderJson = (String) inputMap.get(Constants.ORDER_JSON_KEY);
		Map<String, Object> jsonObject = null;
		try {
			jsonObject = new ObjectMapper().readValue(orderJson, Map.class);
		} catch (JsonParseException e) {
			logger.error("Exception during updateGiftItemPurchaseDetails");
			e.printStackTrace();
			response.getErrorMessages().add("Please provide valid json input");
		} catch (JsonMappingException e) {
			logger.error("Exception during updateGiftItemPurchaseDetails");
			e.printStackTrace();
			response.getErrorMessages().add("Please provide valid json input");
		} catch (IOException e) {
			logger.error("Exception during updateGiftItemPurchaseDetails");
			e.printStackTrace();
			response.getErrorMessages().add("Please provide valid json input");
		}
		logger.info("jsonObject=" + jsonObject);
		if (jsonObject != null) {
			try {
				String giftRegistryItems = null;
				Map<String, Object> orderJsonObject = (Map<String, Object>) jsonObject.get("order");
				giftRegistryItems = orderJsonObject != null ? (String) orderJsonObject.get("itemsAddedFromGiftRegistry")
						: (String) jsonObject.get("itemsAddedFromGiftRegistry");
				logger.info("giftRegistryItems=" + giftRegistryItems);
				if (!GiftRegistryTools.isBlank(giftRegistryItems)) {
					Map<String, Map<String,String>> giftRegistryItemMap = new ObjectMapper().readValue(giftRegistryItems, Map.class);
					Set<String> skuIds =  giftRegistryItemMap.keySet();
					List<GiftRegistryItemPurchaseDetails> purchaseDetailsList = new ArrayList<GiftRegistryItemPurchaseDetails>();
					for(String skuId : skuIds) {
						Map<String,String> valueMap = giftRegistryItemMap.get(skuId);
						String qty = valueMap.get("quantity");
						String registryId = valueMap.get("registryId");
						GiftRegistryItemPurchaseDetails purchaseDetails = new GiftRegistryItemPurchaseDetails();
						purchaseDetails.setSkuId(skuId);
						purchaseDetails.setReceivedQuantity(Integer.valueOf(qty));
						purchaseDetails.setRegistryId(registryId);
						purchaseDetailsList.add(purchaseDetails);
					}
					
					inputMap.put(Constants.PURCHASE_DETAILS, purchaseDetailsList);
					this.giftDAO.updateGiftItemPurchaseDetails(inputMap, response);
					
					/*String[] giftRegistryItemInputs = giftRegistryItems.split(Constants.GIFT_ITEMS_SEPARATOR);
					if (giftRegistryItemInputs != null && giftRegistryItemInputs.length > 0) {
						List<GiftRegistryItemPurchaseDetails> purchaseDetailsList = new ArrayList<GiftRegistryItemPurchaseDetails>();
						for (String giftRegistryItemInput : giftRegistryItemInputs) {
							String[] giftRegistryItemPurchaseDetails = giftRegistryItemInput
									.split(Constants.GIFT_ITEM_DETAILS_SEPARATOR);
							if (giftRegistryItemPurchaseDetails != null
									&& giftRegistryItemPurchaseDetails.length == 3) {
								GiftRegistryItemPurchaseDetails purchaseDetails = new GiftRegistryItemPurchaseDetails();
								purchaseDetails.setSkuId(giftRegistryItemPurchaseDetails[0]);
								purchaseDetails
										.setReceivedQuantity(Integer.valueOf(giftRegistryItemPurchaseDetails[1]));
								purchaseDetails.setRegistryId(Integer.valueOf(giftRegistryItemPurchaseDetails[2]));
								purchaseDetailsList.add(purchaseDetails);
							}
						}
						
					}*/
				} else {
					logger.info("no gift registry details found in order json");
				}
			} catch (Exception e) {
				logger.error("Exception during updateGiftItemPurchaseDetails");
				e.printStackTrace();
				response.getErrorMessages().add("System error. Please try again");
			}
		}
		setResultInResponseObject(response);
	}

	@Override
	public void getBasicGiftRegistriesByUserId(Map<String, Object> inputMap, GiftRegistryResponse response) {
		try {
			GiftRegistryUser giftRegistryUser = this.giftDAO.getGiftRegistryUser(inputMap, response);
			if (giftRegistryUser != null && response.getErrorMessages().size() <= 0) {
				for (GiftRegistry giftRegistry : giftRegistryUser.getGiftRegistries()) {
					BasicRegistryDetails basicDetails = new BasicRegistryDetails();
					basicDetails.setId((giftRegistry.getId()));
					basicDetails.settype(giftRegistry.getType());
					basicDetails.seteventDate(giftRegistry.getEvent().getEventDate());
					response.getResultObjects().add(basicDetails);
				}
			}
		} catch (Exception ex) {
			logger.error("Exception during getBasicGiftRegistriesByUserId");
			ex.printStackTrace();
			response.getErrorMessages().add("System error. Please try again");
		}
		setResultInResponseObject(response);
	}
	
	

	



	@Override
	public void updateRegistryWithItem(GiftRegistry giftRegistry, Map<String, Object> inputMap,
			GiftRegistryResponse response)
	{
		GiftRegistryTools.validateRegistryObject(response.getErrorMessages(), giftRegistry);
		if (response.getErrorMessages().size() <= 0) {
			try {
				
				 ArrayList<GiftRegistryItem> giftRegistryItem = new  ArrayList<GiftRegistryItem>();
				// GiftRegistry copyRegistry = new GiftRegistry();
				 //copyRegistry = giftRegistry;
		
				  
				 for(GiftRegistryItem item:giftRegistry.getGiftRegistryItems())
				{
				 
				  giftRegistryItem.add(item);
				
				}
				 giftRegistry.getGiftRegistryItems().removeAll(giftRegistryItem) ;
	
			
			    
				
				this.giftDAO.updateRegistry(giftRegistry, inputMap, response);
				
                
				
				inputMap.put(Constants.REGISTRY_ID, giftRegistry.getId());
				
				for(GiftRegistryItem item:giftRegistryItem)
				{	
					
					
					this.giftDAO.updateRegistryItem(item, inputMap, response);		
				}
				
			} catch (Exception ex) {
				logger.error("Exception during updateRegistryWithItem");
				ex.printStackTrace();
				response.getErrorMessages().add("System error. Please try again");
			}
		}
		setResultInResponseObject(response);
		}

	@Override
	public void addPurchaseDetails(GiftRegistryPurchaseDetails purchaseDetails, Map<String, Object> inputMap,
			GiftRegistryResponse response) {
		
			try {
				this.giftDAO.addPurchaseDetails(purchaseDetails, inputMap, response);
			} catch (Exception ex) {
				logger.error("Exception during updateRegistryItem");
				ex.printStackTrace();
				response.getErrorMessages().add("System error. Please try again");
			}
		
		setResultInResponseObject(response);
	}
	
	public void validateEmptyObjects(GiftRegistry giftRegistry) {
		if (giftRegistry != null) {
			if (giftRegistry.getCoRegistrant() == null) {
				giftRegistry.setCoRegistrant(new GiftRegistryRegistrant());
			}
			if (giftRegistry.getEvent() == null) {
				giftRegistry.setEvent(new GiftRegistryEvent());
			}
			if (giftRegistry.getPrimaryRegistrant()== null) {
				giftRegistry.setPrimaryRegistrant(new GiftRegistryRegistrant());
			}
			if (giftRegistry.getPreEventShipping()== null) {
				giftRegistry.setPreEventShipping(new GiftRegistryShipping());
			}
			if (giftRegistry.getPostEventShipping() == null) {
				giftRegistry.setPostEventShipping(new GiftRegistryShipping());
			}
		}
	}

	@Override
	public void getPurchaseReport(Map<String, Object> inputMap, GiftRegistryResponse response) {
		try {
			List<GiftRegistryPurchaseReport> report = this.giftDAO.getPurchaseReport(inputMap, response);
			if (response.getErrorMessages().size() == 0)
			{
				response.getResultObjects().add(report);
			}
		} catch (Exception ex) {
			logger.error("Exception during getPurchaseReport");
			ex.printStackTrace();
			response.getErrorMessages().add("System error. Please try again");
		}
	
	setResultInResponseObject(response);
}
		
		
	}
		
	

	
	




