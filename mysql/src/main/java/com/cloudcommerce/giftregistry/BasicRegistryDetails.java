package com.cloudcommerce.giftregistry;

public class BasicRegistryDetails {
	public String id;
	 
	public String eventDate;
 
	private String type;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String geteventDate() {
		return eventDate;
	}
	
	public void seteventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	
	public String gettype() {
		return type;
	}
	
	public void settype(String type) {
		this.type = type;
	}

}
